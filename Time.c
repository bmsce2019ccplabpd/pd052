#include<stdio.h>
struct time
{
	int h;
	int m;
};
typedef struct time Time;
Time input()
{
	Time t;
	printf("Enter the time (hh mm) = ");
	scanf("%d %d",&t.h,&t.m);
	return t;
}
int min(Time n)
{
	int final,temp;
	temp=n.h*60;
	final=temp+n.m;
	return final;
}
void output(Time a,int b)
{
	printf("The entered time (%d:%d) in minutes = %d",a.h,a.m,b);
}
int main()
{
	Time T;
	int ans;
	T=input();
	ans=min(T);
	output(T,ans);
	return 0;
}