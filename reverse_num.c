#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number = ");
    scanf("%d",&n);
    return n;
}
int reverse(int a)
{
    int reverse=0;
    while (a != 0)
    {
        reverse = reverse * 10;
        reverse = reverse + a%10;
        a = a/10;
    }
    return reverse;
}
void output(int sol)
{
    printf("The number in reverse = %d",sol);
}
int main()
{
    int n,rev;
    n=input();
    rev=reverse(n);
    output(rev);
    return 0;
}