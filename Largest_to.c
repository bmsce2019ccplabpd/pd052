#include<stdio.h>
int input()
{
    int n;
    printf("Enter a number = ");
    scanf("%d",&n);
    return n;
}
int greatest(int x,int y,int z)
{
	int g;
	g=(x>y)?((x>z)?x:z):((y>z)?y:z);
	return g;
}
void output(int l,int m,int n,int o)
{
	printf("The greatest of %d,%d and %d numbers = %d",l,m,n,o);
}
int main()
{
    int p,q,r,g;
    p=input();
    q=input();
    r=input();
    g=greatest(p,q,r);
    output(p,q,r,g);
    return 0;
}	