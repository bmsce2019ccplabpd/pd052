#include <stdio.h>
int main()
{
    int x,y=0,n;
    printf("Enter number of elements in array : ");
    scanf("%d",&n);
    int array[n]; 
    printf("Enter %d elements of the array : \n",n);
    for (int i=0;i<n;i++)
    {
        printf("Enter element %d of the array : ",i);
        scanf("%d",&array[i]);    
    }
    printf("\nEnter a number to search in the array : ");
    scanf("%d",&x);
    for (int i=0;i<n;i++)
    {
        if(array[i]==x)
        {
            printf("The element %d is present at position %d of the array ! :D\n",x,i+1);
            y=1;
            break;
        }
    }
    if (y==0)
    printf("%d is not present in the array.\n",x);
    return 0;
}