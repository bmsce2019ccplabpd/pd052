#include<stdio.h>
struct Fract 
{
    int n;
    int d;
};
typedef struct Fract Fract;
Fract input()
{
    Fract a;
    printf("Enter the fraction = ");
    scanf("%d/%d",&a.n,&a.d);
    return a;
}
Fract add()
{
    Fract b,c;
    int n,d;
    printf("Enter the number of fractions = ");
    scanf("%d",&n);
    b=input();
    for(d=1;d<n;d++)
    {
    c=input();    
    b.n=b.n*c.d+c.n*b.d;
    b.d=b.d*c.d;
    }
    return b;
}
int GCD(int d,int e)
{
    int g;
    while(e!=0)
    {
    g=d%e;
    d=e;
    e=g;
    }
    return d;
}
void output(Fract i)
{
    printf("The sum of is %d/%d\n",i.n,i.d);
}
int main()
{
    Fract m,l=add();
    int n;
    n=GCD(l.n,l.d);
    m.n=l.n/n;
    m.d=l.d/n;
    output(m);
}