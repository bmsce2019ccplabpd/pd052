#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number = ");
	scanf("%d",&n);
	return n;
}
int compute(int p)
{
	int t,sum=0;
	while(p>0)
	{
		t=p%10;
		sum=sum+t;
		p=p-t;
		p=p/10;
	}
	return sum;
}
void output(int l,int m)
{
	printf("The sum of digits of the entered number %d = %d \n",l,m);
}
int main()
{
	int a,b;
	a=input();
	b=compute(a);
	output(a,b);
	return 0;
}