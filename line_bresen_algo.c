#include<stdio.h>
#include<math.h>
#include<ncurses.h>
int main()
{
    int x,x1,y1,x2,y2,y,dx,dy,D;
    initscr();
    cbreak();
    refresh();
    char a='|',b='_',s=' ';
    int ym,xm,starty,startx,left=(int)a,right=(int)a,top=(int)b,bottom=(int)b,tlc=(int)s,trc=(int)s,blc=(int)s,brc=(int)s;
    getmaxyx(stdscr,ym,xm);
    starty=0;
    startx=0;
    printw("Enter the coordinates of starting point of the line (x1 y1)= ");
    scanw("%d %d",&x1,&y1);
    printw("Enter the coordinates of ending point of the line (x2 y2)= ");
    scanw("%d %d",&x2,&y2);
    WINDOW * win = newwin(ym,xm,starty,startx);
    refresh();
    wborder(win,left,right,top,bottom,tlc,trc,blc,brc);
    dx=x2-x1;
    dy=y2-y1;
    D=2*dy-dx;
    y=y1;
    for(x=x1;x<=x2;x++)
    {
        mvwprintw(win,x,y,"*");
        if(D>0)
        {
            y=y+1;
            D=D-2*dx;
        }
        else if(D<=0) 
        {
            D=D+2*dy;
        }
    }
    refresh();
    wrefresh(win);
    getch();
    return 0;
}