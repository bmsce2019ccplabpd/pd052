#include<stdio.h>
#include<string.h>
void input_name(int n,char name[])
{
    printf("\nEnter the name of student %d - ",n);
    scanf("%s",name);
}
int inputp(char nme[],char subject[])
{
	int p;
	printf("Enter the marks of student %s in %s = ",nme,subject);
 	scanf("%d",&p);
 	return p;
}
int greatest(int n,int marks[n][3],int *pos)
{
	int g=0,i;
	for(i=0;i<3;i++)
	{
		if(g<marks[n][i])
		{
			g=marks[n][i];
			*pos=i;
		}
	}
	return g;
}
void output(char name[],int marks,char subj[])
{
	printf("\nThe highest marks of student %s is %d in %s",name,marks,subj);
}
int main()
{
	int marks[5][3],maxmarks[5],sub[5];
	char subjects[3][20]={"physics","chemistry","maths"},names[5][100];
	for(int i=0;i<5;i++) 
	{
		input_name(i+1,names[i]);
		for(int j=0;j<3;j++)
		{
			marks[i][j]=inputp(names[i],subjects[j]);
		}
	}
	for(int i=0;i<5;i++)
	{
			maxmarks[i]= greatest(i,marks,&sub[i]);
	}
	for(int p=0;p<5;p++)
	{
		output(names[p],maxmarks[p],subjects[sub[p]]);
	}
  	return 0;
}