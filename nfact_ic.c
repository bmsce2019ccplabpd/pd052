#include<stdio.h>
int input()
{
    int a;
    printf("Enter the value of n = ");
    scanf("%d",&a);
    return a;
}
int factorial(int a)
{
    int fact=a,i=1;
    while(i!=a)
    {
        fact=fact*(a-i);
        i++;
    }
    return fact;
}
float add(int n)
{
    float sum,r;
    int d;
    for(int i=1;i<=n;i++)
    {
        d=factorial(i);
        r=i/d;
        sum=sum+r;
    }
    return sum;
}
void output(int n,float sum)
{
    printf("The sum of the series 1/1!+....+1/%d! = %.f\n",n,sum);
}
int main()
{
    int n=input();
    float sum=add(n);
    output(n,sum);
}