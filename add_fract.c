#include<stdio.h>
struct Fract 
{
    int n;
    int d;
};
typedef struct Fract Fract;
Fract input()
{
    Fract a;
    printf("Enter the number of fraction = ");
    scanf("%d/%d",&a.n,&a.d);
    return a;
}
Fract add(Fract b,Fract c)
{
    Fract f;
    f.n=b.n*c.d+c.n*b.d;
    f.d=b.d*c.d;
    return f;
}
int GCD(int d,int e)
{
    int g;
    while(e!=0)
    {
    g=d%e;
    d=e;
    e=g;
    }
    return d;
}
void output(Fract g,Fract h,Fract i)
{
    printf("The sum of %d/%d and %d/%d is %d/%d\n",g.n,g.d,h.n,h.d,i.n,i.d);
}
int main()
{
    Fract j,k,l,m;
    int n;
    j=input();
    k=input();
    l=add(j,k);
    n=GCD(l.n,l.d);
    m.n=l.n/n;
    m.d=l.d/n;
    output(j,k,m);
}