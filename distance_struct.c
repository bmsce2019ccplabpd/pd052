#include<stdio.h>
#include<math.h>
struct point
{ 
    float x;
    float y;
};
struct point input_point()
{
    struct point a;
    printf("Enter the x and y coordinates of the point x y = ");
    scanf("%f %f",&a.x,&a.y);
    return a;
}
float distance(struct point b,struct point c)
{
    float d,e,r;
    d=b.x-c.x;
    e=b.y-c.y;
    r=sqrt(d*d+e*e);
    return r;
}
float output(struct point p,struct point q,float s)
{
    printf("The coordinates of the first point are (x,y) = (%f,%f)\n",p.x,p.y);
    printf("The coordinates of the second point are (x,y) = (%f,%f)\n",q.x,q.y);
    printf("The distance between the given two points are = %f units\n",s);
}
float main()
{
    struct point f,g;
    float h;
    f=input_point();
    g=input_point();
    h=distance(f,g);
    output(f,g,h);
}