#include<stdio.h>
struct fract 
{
    int n;
    int d;
};
typedef struct fract Fract;
int input()
{
    int a;
    printf("Enter the value of n = ");
    scanf("%d",&a);
    return a;
}
int factorial(int a)
{
    int fact=a,i=1;
    while(i!=a)
    {
        fact=fact*(a-i);
        i++;
    }
    return fact;
}
Fract add(int n)
{
    Fract a,sum;
    sum.n=1;
    sum.d=1;
    for(int i=2;i<=n;i++)
    {
        a.n=1;
        a.d=factorial(i);
        sum.n=a.d*sum.n + a.n*sum.d;
        sum.d=sum.d*a.d;
    }
    return sum;
}
void output(int n,Fract sum,float total)
{
    printf("The sum of the series 1/1!+....+1/%d! = %d/%d = %f\n",n,sum.n,sum.d,total);
}
int main()
{
    Fract sum;
    float total;
    int n=input();
    sum=add(n);
    total=sum.n/sum.d;
    output(n,sum,total);
}