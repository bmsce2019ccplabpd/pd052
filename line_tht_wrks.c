#include<stdio.h>
#include<math.h>
#include<ncurses.h>
int main()
{
    int x,y,x1,y1,x2,y2;
    initscr();
    cbreak();
    refresh();
    char a='|',b='_',s=' ';
    int ym,xm,starty,startx,left=(int)a,right=(int)a,top=(int)b,bottom=(int)b,tlc=(int)s,trc=(int)s,blc=(int)s,brc=(int)s;
    getmaxyx(stdscr,ym,xm);
    starty=0;
    startx=0;
    printw("Enter the coordinates of starting point of the line (x1 y1)= ");
    scanw("%d %d",&x1,&y1);
    printw("Enter the coordinates of ending point of the line (x2 y2)= ");
    scanw("%d %d",&x2,&y2);
    WINDOW * win = newwin(ym,xm,starty,startx);
    refresh();
    wborder(win,left,right,top,bottom,tlc,trc,blc,brc);
    
    
    int dx, dy, p;
    dx=x2-x1;
	dy=y2-y1;
    x=x1;
	y=y1;
    p=2*dy-dx;
    while(x<x2)
	{
		if(p>=0)
		{
			mvwprintw(win,x,y,"*");
			y=y+1;
			p=p+2*dy-2*dx;
		}
		else
		{
			mvwprintw(win,x,y,"*");
			p=p+2*dy;
		}
		x=x+1;
	}
	
	
    refresh();
    wrefresh(win);
    getch();
    return 0;
}