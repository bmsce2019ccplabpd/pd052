#include<stdio.h>
#include<ncurses.h>
#include<math.h>
struct point
{
    int x;
    int y;
};
typedef struct point Point;
Point input_line()
{
    Point a;
    printw("Enter the coordinates of the line (x y) = ");
    scanw("%d %d",&a.x,&a.y);
    return a;
}
Point input_crle()
{
    Point b;
    printw("Enter the coordinates of the centre of the circle (x y) = ");
    scanw("%d %d",&b.x,&b.y);
    return b;
}
int input_rad()
{
    int rad;
    printw("Enter the value of radius of the circle = ");
    scanw("%d",&rad);
    return rad;
}
int main()
{
    Point a,b,c;
    int r,r2,x,y,dx,dy,D,X,Y;
    initscr();
    cbreak();
    refresh();
    char p='|',q='_',s=' ';
    int ym,xm,starty,startx,left=(int)p,right=(int)p,top=(int)q,bottom=(int)q,tlc=(int)s,trc=(int)s,blc=(int)s,brc=(int)s;
    getmaxyx(stdscr,ym,xm);
    starty=0;
    startx=0;
    a=input_line();
    b=input_line();
    c=input_crle();
    r=input_rad();
    WINDOW * win = newwin(ym,xm,starty,startx);
    refresh();
    wborder(win,left,right,top,bottom,tlc,trc,blc,brc);
    r2=r*r;
    for(y=c.y-r;y<=c.y+r;y++)
    {
        x=sqrt(r2-pow(y-c.y,2))+c.x;
        mvwprintw(win,y,x,"*");
        x=x-2*(x-c.x);
        mvwprintw(win,y,x,"*");
    }
    dx=b.x-a.x;
    dy=b.y-a.y;
    D=2*dy-dx;
    Y=a.y;
    for(X=a.x;X<=b.x;X++)
    {
        mvwprintw(win,X,Y,"*");
        if(D>0)
        {
            Y=Y+1;
            D=D-2*dx;
        }
        else if(D<=0) 
        {
            D=D+2*dy;
        }
    }
    wrefresh(win);       
    refresh();
    getch();
    return 0;
}