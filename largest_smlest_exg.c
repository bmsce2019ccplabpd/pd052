#include<stdio.h>
int input(int ar[])
{
    int n;
    printf("Enter the number of values enter = ");
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        printf("Enter the value %d = ",i+1);
        scanf("%d",&ar[i]);
    }
    return n;
}
int largest(int n,int l[],int *pos)
{
    int large=l[0];
    for(int i=1;i<n;i++)
    {
        if(l[i]>large)
        {
            large=l[i];
            *pos=i;
        }
    }
    return large;
}
int smallest(int n,int s[],int *pos)
{
    int small=s[0];
    for(int i=1;i<n;i++)
    {
        if(s[i]<small)
        {
            small=s[i];
            *pos=i;
        }
    }
    return small;
}
int swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
void output(int a,int b,int array[],int n,int x,int y)
{
    printf("The largest value = %d and smallest values = %d\n",a,b);
    printf("The entered array before swapping:\n");
    for(int i=0;i<n;i++)
    {
        printf("%d ",array[i]);
    }
    printf("\n");
    swap(&array[x],&array[y]);
    printf("The array after swapping:\n");
    for(int i=0;i<n;i++)
    {
        printf("%d ",array[i]);
    }
}
int main()
{
    int lar,sml,n,array[n],smlps,larps;
    n=input(array);
    lar=largest(n,array,&larps);
    sml=smallest(n,array,&smlps);
    output(lar,sml,array,n,larps,smlps)
    return 0;
}