def inputn():
    n=int(input("Enter the number of values for the array : "))
    return n
def inpute(p):
    x=int(input("Enter element %d of the array : "%(p)))
    return x
def largest(array,n):
    l=array[0]
    lpos=0
    for i in range(0,n):
        if(l<array[i]):
            l=array[i]
            lpos=i
    return l,lpos
def smallest(array,n):
    s=array[0]
    spos=0
    for i in range(0,n):
        if(s>array[i]):
            s=array[i]
            spos=i
    return s,spos
def swap(a,b):
    temp=a
    a=b
    b=temp
    return a,b 
def outputa(n,array,l,s,lpos,spos):
    print("\nThe largest value = %d and smallest values = %d\n"%(l,s))
    print("The entered array before swapping:\n")
    print(array)
    array[lpos],array[spos]=swap(array[lpos],array[spos])
    print("\nThe entered array after swapping:\n")
    print(array)
def main():
    a=[]
    n=inputn()
    for i in range(0,n):
        x=inpute(i+1)
        a.append(x)
    lar,lpos=largest(a,n)
    sml,spos=smallest(a,n)
    outputa(n,a,lar,sml,lpos,spos)
main()