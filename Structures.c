#include<stdio.h>
#include<string.h>
struct date_of_birth
{
	int d;
	int m;
	int yr;
};
typedef struct date_of_birth dob;
struct employee
{
	char name[100];
	dob age;
	char blood[4];
	int expe;
	int weight;
	int height;
	char clg[100];
	char num[100];
	char anum[100];
	char sex[10];
};
typedef struct employee Em;
int inputn()
{
    int n;
    printf("Enter the number of employees = ");
    scanf("%d",&n);
    return n;
}
Em input()
{
    Em e;
    printf("\nEnter the name of the employee - ");
    scanf("%s",e.name);
    printf("Enter the date of birth of the employee (dd mm yy)= ");
	scanf("%d %d %d",&e.age.d,&e.age.m,&e.age.yr);
	printf("Enter the blood group of the employee - ");
	scanf("%s",e.blood);
	printf("Enter the experience of the employee - ");
	scanf("%d",&e.expe);
	printf("Enter the weight of the employee = ");
	scanf("%d",&e.weight);
    printf("Enter the height of the employee = ");
	scanf("%d",&e.height);
	printf("Enter the name of the college of the employee - ");
    scanf("%s",e.clg);
    printf("Enter the phone number of the employee - ");
	scanf("%s",e.num);
	printf("Enter the aadhar number of the employee - ");
	scanf("%s",e.anum);
	printf("Enter the sex of the employee - ");
	scanf("%s",e.sex);
	return e;
}
void output(int n,Em x[n])
{
	for(int i=0;i<n;i++)
	{
		printf("\nEmployee %d :- \n",i+1);
		printf("1)The name of the employee - %s\n",x[i].name);
		printf("2)The date of birth of the employee - %d/%d/%d\n",x[i].age.d,x[i].age.m,x[i].age.yr);
		printf("3)The blood group of the employee - %s\n",x[i].blood);
		printf("4)The experience of the employee - %d\n",x[i].expe);
		printf("5)The weight of the employee = %d\n",x[i].weight);
		printf("6)The height of the employee = %d\n",x[i].height);
		printf("7)The name of the college of the employee = %s\n",x[i].clg);
		printf("8)The phone number of the employee = %s\n",x[i].num);
		printf("9)The aadhaar number of the employee = %s\n",x[i].anum);
		printf("10)The sex of the employee = %s\n",x[i].sex);
	}
}
int main()
{
	int n=inputn();
	Em h[n];
	for (int i=0;i<n;i++)
	{
	    h[i]=input();
	}
	output(n,h);
	return 0;
}