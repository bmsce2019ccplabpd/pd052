#include<stdio.h>
#include<math.h>
#include<ncurses.h>
float line(int X,int Y,int X1,int Y1,int X2,int Y2)
{
    float a,d,e,h,g,i,f;
    d=X2-X1;
    e=Y2-Y1;
    f=e/d;
    g=Y-Y1;
    h=X-X1;
    i=f*h;
    a=i/g;
    return a;
}
int main()
{
    int m,x,y,x1=2,y1=1,x2=5,y2=7;
    initscr();
    cbreak();
    noecho();
    refresh();
    char a='|',b='_',s=' ';
    int ym,xm,starty,startx,left=(int)a,right=(int)s,top=(int)s,bottom=(int)b,tlc=(int)s,trc=(int)s,blc=(int)s,brc=(int)s;
    getmaxyx(stdscr,ym,xm);
    starty=0;
    startx=0;
    WINDOW * win = newwin(ym,xm,starty,startx);
    refresh();
    wborder(win,left,right,top,bottom,tlc,trc,blc,brc);
    for(x=x1;x<=x2;x++)
    {
        for(y=y1;y<=y2;y++)
        {
            if(line(x,y,x1,y1,x2,y2) == 1)
            {mvwprintw(win,y,x,"*");}
            else
            {mvwprintw(win,y,x," ");}
        }
    }
    wrefresh(win);
    getch();
    return 0;
}