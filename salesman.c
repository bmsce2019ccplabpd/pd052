#include<stdio.h>
int input(int n,int m)
{
	int s;
	printf("Enter the sales of salesman %d of product %d = ",n,m);
 	scanf("%d",&s);
 	return s;
}
void salesman(int array[5][3],int sm[5])
{
	int sum;
	for(int i=0;i<5;i++)
	{
	    sum=0;
		for(int j=0;j<3;j++)
		{
		    sum=sum+array[i][j];
		}
		sm[i]=sum;
	}
}
void salesprod(int array[5][3],int sp[3])
{
	int sum;
	for(int i=0;i<3;i++)
	{
		sum=0;
		for(int j=0;j<5;j++)
		{
		    sum=sum+array[j][i];
		}
		sp[i]=sum;
	}
}
void output(int sman[5],int sprod[3])
{
	for(int i=0;i<5;i++)
	{
	    printf("The total sales by salesman %d = %d\n",i+1,sman[i]);
	}
	for(int j=0;j<3;j++)
	{
	    printf("The total sales of product %d = %d\n",j+1,sprod[j]);
	}
}
int main()
{
	int sales[5][3],sm[5],sp[3];
	for(int i=0;i<5;i++) 
	{
		for(int j=0;j<3;j++)
		{
			sales[i][j]=input(i+1,j+1);
		}
	}
	salesman(sales,sm);
	salesprod(sales,sp);
	output(sm,sp);
	return 0;
}