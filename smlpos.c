#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number of values = ");
    scanf("%d",&n);
    return n;
}
int smallest(int n,int s[],int *pos)
{
    int small=s[0],i;
    for(i=1;i<n;i++)
    {
        if(s[i]<small)
        {
            small=s[i];
            *pos=i;
        }
    }
    return small;
}
void output(int a,int b)
{
    printf("The smallest value = %d and its positions is %d\n",a,b);
}
int main()
{
    int sml,n=input(),smlpos;
    int array[n];
    for(int i=0;i<n;i++)
    {
        printf("Enter the value %d = ",i+1);
        scanf("%d",&array[i]);
    }
    sml=smallest(n,array,&smlpos);
    output(sml,smlpos);
    return 0;
}