#include<stdio.h>
float input()
{
    float f;
    printf("Enter the temperature in fahrenheit : ");
    scanf("%f",&f);
    return f;
}
float convert(float f)
{
    float c;
    c=(f-32)*5/9;
    return c;
}
void output(float f,float c)
{
    printf("The entered temperature %.3f(deg.F) in degree celsius = %.3f",f,c);
}
int main()
{
    float fahrenheit,celsius;
    fahrenheit=input();
    celsius=convert(fahrenheit);
    output(fahrenheit,celsius);
    return 0;
}