#include<stdio.h>
int input()
{
	int n;
	printf("Enter a number = ");
	scanf("%d",&n);
	return n;
}
int swap(int *a,int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}
void output(int p,int q)
{
	printf("The numbers swapped = %d and %d",p,q);
}
int main()
{
	int x,y;
	x=input();
	y=input();
	swap(&x,&y);
	output(x,y);
	return 0;
}