#include<stdio.h>
int inputn()
{
    int n;
    printf("Enter the number of values = ");
    scanf("%d",&n);
    return n;
} 
void input(int n,int ar[])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the value = ");
        scanf("%d",&ar[i]);
    }
}
void delete(int *n,int arr[])
{
    int pos;
    printf("Enter the position you want to delete = ");
    scanf("%d",&pos);
    for(int i=pos;i<*n;i++)
    {
        arr[i]=arr[i+1];
    }
    *n=*n-1;
}
void output(int a[],int n)
{
    printf("The new array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\n",a[i]);
    }
}
int main()
{
    int n=inputn();
    int array[n];
    input(n,array);
    delete(&n,array);
    output(array,n);
    return 0;
}