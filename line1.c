#include<stdio.h>
#include<math.h>
#include<ncurses.h>
int slope(int X1,int Y1,int X2,int Y2)
{
    float d,e,h,g,i,f;
    int a;
    d=X2-X1;
    e=Y2-Y1;
    a=e/d;
    return a;
}
int main()
{
    int m,x,y,x1,y1,x2,y2;
    initscr();
    cbreak();
    refresh();
    char a='|',b='_',s=' ';
    int ym,xm,starty,startx,left=(int)a,right=(int)a,top=(int)b,bottom=(int)b,tlc=(int)s,trc=(int)s,blc=(int)s,brc=(int)s;
    getmaxyx(stdscr,ym,xm);
    starty=0;
    startx=0;
    printw("Enter the coordinates of starting point of the line (x1 y1)= ");
    scanw("%d %d",&x1,&y1);
    printw("Enter the coordinates of ending point of the line (x2 y2)= ");
    scanw("%d %d",&x2,&y2);
    getch();
    WINDOW * win = newwin(ym,xm,starty,startx);
    refresh();
    wborder(win,left,right,top,bottom,tlc,trc,blc,brc);
    x=x1;
    y=y1;
    mvwprintw(win,x,y,"*");
    m=slope(x1,y1,x2,y2);
    if(m<=0)
    {m=-1*m;}
    else {m=m;}
    if(m<=1)
    {
        while(x<=x2)
        {
            x=x++;
            y=y+m;
            mvwprintw(win,x,y,"*");
        }
    }
    else
    {
        while(y<=y2)
        {
            y=y++;
            x=x/m;
            mvwprintw(win,x,y,"*");
        }
    }
    wrefresh(win);
    getch();
    return 0;
}