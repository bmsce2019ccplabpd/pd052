#include <stdio.h>
int input_array(int numbers[])
{
  int n;
  printf("Enter the number of elements you want in the array : ");
  scanf("%d",&n);
  printf("\n");
  for(int i=0; i<n; i++)
  {
    printf("Enter element %d : ",i);
    scanf("%d",&numbers[i]);
  }
  return n;
}

void partition(int n,int numbers[])
{
  int pivot = numbers[0], i, j = n - 1, temp = 0;
  for(i=1; i<=j; i++)
  {
    while(j>i && numbers[i]>pivot)
    {
      if( pivot >= numbers[j] )
      {
          temp = numbers[j];
          numbers[j] = numbers[i];
          numbers[i] = temp;
          i++;
      }
      j--;
    }
  }
  if(n==i && numbers[j]<=pivot)
  {
    numbers[0]=numbers[j];
    numbers[j]=pivot;
  }
  else
  {
    numbers[0] = numbers[i-2];
    numbers[i-2] = pivot;
  }
}

void output(int n, int numbers[])
{
    printf("\n");
    for(int i=0; i<n; i++)
    {
        printf("%d ",numbers[i]);
    }
}
int main()
{
  int size, sol[100];
  size = input_array(sol);
  partition(size, sol);
  output(size, sol);
}
