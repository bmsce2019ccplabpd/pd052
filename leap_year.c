#include <stdio.h>
int input()
{
    int x;
    printf("Enter a year to check if it is a leap year : ");
    scanf("%d",&x);
    return x;
}
int leap_year(int year)
{
    if (year%400 == 0) 
    {
        return 1;
    }
    else if (year%100 == 0)
    {
        return 0;
    }
    else if (year%4 == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
void output(int yr,int n)
{
    if(n==1)
    {
        printf("%d is a leap year ! :D",yr);
    }
    else 
    {
        printf("%d is not a leap year :( ",yr);
    }
}
int main()
{
    int y=input(),n;
    n=leap_year(y);
    output(y,n);
    return 0;
}