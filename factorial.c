#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number = ");
    scanf("%d",&n);
    return n;
}
int factorial(int a)
{
    int fact=a,i=1;
    while(i!=a)
    {
        fact=fact*(a-i);
        i++;
    }
    return fact;
}
void output(int sol)
{
    printf("The factorial of entered number = %d\n",sol);
}
int main()
{
    int n,f;
    n=input();
    f=factorial(n);
    output(f);
    return 0;
}