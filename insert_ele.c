#include<stdio.h>
int inputn()
{
    int n;
    printf("Enter the number of values = ");
    scanf("%d",&n);
    return n;
} 
void input(int n,int ar[])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the value = ");
        scanf("%d",&ar[i]);
    }
}
void insert(int *n,int arr[])
{
    int x,pos;
    printf("Enter the position you want to insert the number = ");
    scanf("%d",&pos);
    printf("Enter the number you want to insert = ");
    scanf("%d",&x);
    for(int i=*n-1;i>=pos;i--)
    {
        arr[i+1]=arr[i];
    }
    *n=*n+1;
    arr[pos]=x;
}
void output(int a[],int n)
{
    printf("The new array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\n",a[i]);
    }
}
int main()
{
    int n=inputn();
    int array[n];
    input(n,array);
    insert(&n,array);
    output(array,n);
    return 0;
}