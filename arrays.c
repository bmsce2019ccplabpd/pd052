#include<stdio.h>
int main()
{
    int n;
    printf("Enter the number of values of the array : ");
    scanf("%d",&n);
    int array[n];
    for(int i=0;i<n;i++)
    {
        printf("Enter value of element %d of the array : ",i+1);
        scanf("%d",&array[i]);
    }
    printf("\nThe elements of entered array are : ");
    for(int i=0;i<n;i++)
    {
        printf("%d ",array[i]);
    }
    return 0;
}